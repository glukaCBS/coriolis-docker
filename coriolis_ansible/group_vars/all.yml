---
#
# The options in this file can be overridden in 'config.yml'
#

build_dir: "{{ playbook_dir }}/build"

coriolis_certificate_store: "/etc/coriolis-certificates"
coriolis_certtificate_fqdn: ""
coriolis_certificate_extra_sans: []

coriolis_appliance_tls_certificate: "{{coriolis_certificate_store}}/certificate.pem"
coriolis_appliance_tls_key: "{{coriolis_certificate_store}}/key.pem"
coriolis_appliance_tls_cacert: "{{coriolis_certificate_store}}/cacert.pem"
coriolis_appliance_tls_cert_bundle: "{{coriolis_certificate_store}}/bundle.pem"
coriolis_appliance_tls_combined: "{{coriolis_certificate_store}}/combined.pem"

bind_address: 127.0.0.1
endpoint_base_address: "{{ coriolis_certtificate_fqdn | default(bind_address, true) }}"

enable_coriolis_licensing_server: false

kolla_branch: 9.0.0
kolla_openstack_release: 2023.1
kolla_docker_images_tag: latest

docker_pull_images: true
docker_registry: registry.cloudbase.it
docker_namespace: appliance

default_coriolis_docker_images_tag: latest
coriolis_common_docker_image_tag: "{{ default_coriolis_docker_images_tag }}"

coriolis_api_docker_image_tag:              "{{ default_coriolis_docker_images_tag }}"
coriolis_conductor_docker_image_tag:        "{{ default_coriolis_docker_images_tag }}"
coriolis_scheduler_docker_image_tag:        "{{ default_coriolis_docker_images_tag }}"
coriolis_logger_docker_image_tag:           "{{ default_coriolis_docker_images_tag }}"
coriolis_compressor_docker_image_tag:       "{{ default_coriolis_docker_images_tag }}"
coriolis_licensing_server_docker_image_tag: "{{ default_coriolis_docker_images_tag }}"
coriolis_licensing_ui_docker_image_tag:     "{{ default_coriolis_docker_images_tag }}"
coriolis_metal_hub_docker_image_tag:        "{{ default_coriolis_docker_images_tag }}"
coriolis_transfer_cron_docker_image_tag:    "{{ default_coriolis_docker_images_tag }}"
coriolis_minion_manager_docker_image_tag:   "{{ default_coriolis_docker_images_tag }}"
coriolis_worker_docker_image_tag:           "{{ default_coriolis_docker_images_tag }}"
coriolis_web_proxy_docker_image_tag:        "{{ default_coriolis_docker_images_tag }}"
coriolis_web_docker_image_tag:              "{{ default_coriolis_docker_images_tag }}"

coriolis_console_editor_docker_image_tag: "{{ default_coriolis_docker_images_tag }}"
coriolis_common_docker_image_url: "{{ docker_registry }}/{{ docker_namespace }}/coriolis-common:{{ coriolis_common_docker_image_tag }}"

coriolis_debug: true

coriolis_port: 7667
coriolis_base_url: "https://{{ endpoint_base_address }}:{{ coriolis_port }}/v1"
coriolis_web_ui_bind_address: "127.0.0.1"
coriolis_web_ui_base_url: "http://{{ coriolis_web_ui_bind_address }}:3000"

coriolis_config_dir: /etc/coriolis
coriolis_log_dir: /var/log/coriolis
coriolis_vmware_vix_disklib_log_dir: "{{ coriolis_log_dir }}/vmware-root"
coriolis_vmware_vix_disklib_config_path: "{{ coriolis_config_dir }}/vixdisklib.conf"
appliance_coriolis_opt_dir: /opt/coriolis
coriolis_vmware_vix_disklib_dir: "{{ appliance_coriolis_opt_dir }}/vmware-vix-disklib"
coriolis_hyperv_share_local_dir: "{{ appliance_coriolis_opt_dir }}/hyperv-exports"
coriolis_export_dir: "{{ appliance_coriolis_opt_dir }}/export"
coriolis_profiling_dir_container: "{{ appliance_coriolis_opt_dir }}/profiling"
coriolis_docker_image_dir: /tmp/coriolis_docker_images
coriolis_docker_image_timeout: 300

coriolis_export_providers: ["openstack", "oracle-vm", "opc", "azure", "scvmm", "vmware", "aws", "metal"]
coriolis_import_providers: ["openstack", "oracle-vm", "opc", "azure", "scvmm", "oci", "aws", "vmware", "ovirt"]

coriolis_logger_socket_dir: /var/run/coriolis-logger
coriolis_logger_socket_name: logger.socket
coriolis_logger_socket_file: "{{ coriolis_logger_socket_dir }}/{{ coriolis_logger_socket_name }}"
coriolis_logger_volume_name: coriolis-logger
coriolis_logger_port: 9998
coriolis_logger_endpoint: "https://{{ endpoint_base_address }}:{{ coriolis_logger_port }}/api/v1"
coriolis_logger_ws_url: "wss://{{ endpoint_base_address }}:{{ coriolis_logger_port }}/api/v1/ws"

coriolis_licensing_server_port: 37667
coriolis_licensing_server_endpoint: "https://{{ endpoint_base_address }}:{{ coriolis_licensing_server_port }}/v2"
coriolis_licensing_ui_port: 3010
coriolis_licensing_ui_endpoint: "https://{{ endpoint_base_address }}:{{ coriolis_licensing_ui_port }}/licensing-ui"

# We can only specify one global "[oslo_concurrency] lock_path" in
# coriolis.conf so we will map separate directories on the host to
# the same subpath within all of the containers:
coriolis_locks_dir_containers: "{{ appliance_coriolis_opt_dir }}/locks"
coriolis_locks_dir_host: "{{ appliance_coriolis_opt_dir }}/locks"
coriolis_conductor_locks_dir_host: "{{ coriolis_locks_dir_host }}/conductor"
coriolis_minion_manager_locks_dir_host: "{{ coriolis_locks_dir_host }}/minion-manager"

# Mountpoints on the coriolis-console-editor container:
coriolis_console_editor_extra_prompt_file: "{{ appliance_coriolis_opt_dir }}/extra-coriolis-editor-prompt.txt"
kolla_admin_openrc_filepath: "/etc/kolla/admin-openrc.sh"
appliance_etc_hosts_filepath: "/etc/hosts"
appliance_etc_resolvconf_filepath: "/etc/resolv.conf"
appliance_networking_config_filepath: "/etc/netplan"
coriolis_ansible_install_dir: "/root/coriolis-docker"
coriolis_ansible_main_config_file: "{{ coriolis_ansible_install_dir }}/config.yml"
coriolis_ansible_docker_config_file: "{{ coriolis_ansible_install_dir }}/docker-images-config.yml"
coriolis_logger_conf_file: "/etc/coriolis-logger/coriolis-logger.toml"
coriolis_console_editor_mountdirs:
  - "{{ kolla_admin_openrc_filepath }}:{{ kolla_admin_openrc_filepath }}:rw"
  - "{{ appliance_etc_hosts_filepath }}:{{ appliance_etc_hosts_filepath }}:rw"
  - "{{ appliance_etc_resolvconf_filepath }}:{{ appliance_etc_resolvconf_filepath }}:rw"
  - "{{ appliance_networking_config_filepath }}:{{ appliance_networking_config_filepath }}:rw"
  - "{{ appliance_coriolis_opt_dir }}:{{ appliance_coriolis_opt_dir }}:rw"
  - "{{ coriolis_config_dir }}:{{ coriolis_config_dir }}:rw"
  - "{{ coriolis_logger_conf_file }}:{{ coriolis_logger_conf_file }}:rw"
  - "{{ coriolis_log_dir }}:{{ coriolis_log_dir }}:rw"
  - "{{ coriolis_ansible_main_config_file }}:{{ coriolis_ansible_main_config_file }}:rw"
  - "{{ coriolis_ansible_docker_config_file }}:{{ coriolis_ansible_docker_config_file }}:rw"

coriolis_metal_hub_port: 9900
coriolis_metal_hub_endpoint: "https://{{ endpoint_base_address }}:{{ coriolis_metal_hub_port }}/api/v1"
coriolis_metal_hub_config_dir: /etc/coriolis-metal
coriolis_metal_hub_certs_dir: "{{ coriolis_metal_hub_config_dir }}/certs"
coriolis_metal_hub_ca_cert_path: "{{ coriolis_metal_hub_certs_dir }}/ca-pub.pem"
coriolis_metal_hub_server_cert_path: "{{ coriolis_metal_hub_certs_dir }}/srv-pub.pem"
coriolis_metal_hub_server_key_path: "{{ coriolis_metal_hub_certs_dir }}/srv-key.pem"
coriolis_metal_hub_client_cert_path: "{{ coriolis_metal_hub_certs_dir }}/client-pub.pem"
coriolis_metal_hub_client_key_path: "{{ coriolis_metal_hub_certs_dir }}/client-key.pem"
coriolis_metal_hub_endpoint_cert_paths:
  - { fact: metal_hub_endpoint_ca_certificate, path: "{{ coriolis_metal_hub_ca_cert_path }}" }
  - { fact: metal_hub_endpoint_certificate, path: "{{ coriolis_metal_hub_client_cert_path }}" }
  - { fact: metal_hub_endpoint_key, path: "{{ coriolis_metal_hub_client_key_path }}" }
default_appliance_metal_hub_endpoint_name: appliance-metal-hub
step_ca_home: /etc/step

# Replaces coriolis_worker_hostname to ensure backward compatibility
# with existing appliances that use it in order to allow for external workers
# being deployed. Required here for ansible precedence due to old variable
coriolis_worker_service_hostname: "{{ 'coriolis-worker-' + inventory_hostname | replace( '.', '-' ) if ansible_connection != 'local' else coriolis_worker_hostname }}"
